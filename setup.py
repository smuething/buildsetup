from setuptools import setup, find_packages

setup(
    name = 'buildsetup',

    version = '1.0.0',

    description  = 'A tool for managing multiple builds (in particular for DUNE projects)',

    url = 'http://conan2.iwr.uni-heidelberg.de/git/smuething/buildsetup',

    author = 'Steffen Müthing',
    author_email = 'steffen.muething@iwr.uni-heidelberg.de',

    license='BSD',

    packages=find_packages(),

    install_requires = [
        'layered-yaml-attrdict-config',
        'PyYAML',
        'enum34',
        ],

    entry_points={
        'console_scripts' : [
            'run-buildsetup=buildsetup.scripts:buildsetup',
            ],
        'buildsetup.plugins' : [
            'lmod=buildsetup.plugins.lmod:load',
            ],
        },
)
