from __future__ import print_function

import os, sys, textwrap

from .exceptions import *
from . import Configuration, Configurator


def configure():

    rerun = sys.argv[1] == '__rerun'
    if rerun:
        sys.argv[1:2] = []

    cmdfile = sys.argv[1]
    sys.argv[1:2] = []

    from .configurator import Configurator

    cfg = Configuration()


    c = Configurator(cfg,cmdfile,rerun)

    c.parse_keys()

    c.set_value('CC',cmake_var='CMAKE_C_COMPILER',path=True)
    c.set_value('CXX',cmake_var='CMAKE_CXX_COMPILER',path=True)
    c.set_value('FC',cmake_var='CMAKE_Fortran_COMPILER',path=True)

    c.set_list('CFLAGS',keys=('cflags','extra_cflags'),cmake_var='CMAKE_C_FLAGS')
    c.set_list('CXXFLAGS',keys=('cflags','cxxflags'),cmake_var='CMAKE_CXX_FLAGS')
    c.set_list('FFLAGS',keys=('fflags',),cmake_var='CMAKE_Fortran_FLAGS')
    c.set_list('CFLAGS_DEBUG',keys=('cflags','extra_cflags'),subkey='debug',cmake_var='CMAKE_C_FLAGS_DEBUG')
    c.set_list('CXXFLAGS_DEBUG',keys=('cflags','cxxflags'),subkey='debug',cmake_var='CMAKE_CXX_FLAGS_DEBUG')
    c.set_list('FFLAGS_DEBUG',keys=('fflags',),subkey='debug',cmake_var='CMAKE_Fortran_FLAGS_DEBUG')
    c.set_list('CFLAGS_RELEASE',keys=('cflags','extra_cflags'),subkey='release',cmake_var='CMAKE_C_FLAGS_RELEASE')
    c.set_list('CXXFLAGS_RELEASE',keys=('cflags','cxxflags'),subkey='release',cmake_var='CMAKE_CXX_FLAGS_RELEASE')
    c.set_list('FFLAGS_RELEASE',keys=('fflags',),subkey='release',cmake_var='CMAKE_Fortran_FLAGS_RELEASE')

    c.set_value('CMAKE_BUILD_TYPE',cmake_var='CMAKE_BUILD_TYPE')

    c.set_list('LDFLAGS',keys=('ldflags',))

    c.finalize()

    env = dict(os.environ)
    env.update(c)

    c.report(sys.stderr)

    c.load_build_data()
    if not c.need_rerun:
        c.save_build_data(sys.stderr)

    #cfg.dump(sys.stdout)

    #args = ["/bin/echo","dune-common/bin/dunecontrol","--builddir={}".format(c.build_dir),"--opts=opts/standard.opts"]
    #args.extend(sys.argv[1:])
    #os.execve("/bin/echo",args,env)

    #args = ["/bin/bash","-c","export"]
    #os.execve("/bin/bash",args,env)

    c.run(sys.stderr)



def bash_complete():

    cur = sys.argv[1]
    prev = sys.argv[2]
    pos = int(sys.argv[3])
    cmdline = sys.argv[4]
    splitline = sys.argv[5:]
    #print('\ncur={} prev={} pos={} cmdline="{}" splitline={}\n'.format(cur,prev,pos,cmdline,splitline),file=sys.stderr)

    return_command = ':'

    try:
        cfg = Configuration()
        Configurator.register_keys(cfg)

        options = cfg.configkeys.viewkeys()

        if prev in options and splitline[pos] == '=':
            values = True
            prefix = ''
            keyname = prev
        elif prev == '=' and splitline[pos-2] in options:
            values = True
            prefix = cur
            keyname = splitline[pos-2]
        else:
            values = False

        if values:
            key = cfg.configkeys[keyname]

            if key.repo:
                # project is special, we need to parse it to look up available variants
                if keyname != 'project':
                    try:
                        import shlex
                        cfg.parse_key('project',argv=shlex.split(cmdline))
                    except:
                        pass

                repo = key.repo() if key.lazy_repo else key.repo
                for n,v in repo.viewitems():
                    if n.startswith(prefix) and not ('abstract' in v and v.abstract):
                        print(n)

            else:
                for c in key.completions():
                    if c.startswith(prefix):
                        print(c)

        else:
            for c in options:
                if c.startswith(cur) and not c in splitline[:pos]:
                    print(c + '=')
            # don't add a space after the equals sign
            return_command = 'compopt -o nospace'
    except:
        import traceback
        traceback.print_exc(sys.stderr)
    finally:
        print(return_command)


def setup_completion():
    shellcode="""\
    _buildsetup(){
        local reply="$(python -c 'from buildsetup.scripts import bash_complete; bash_complete()' \"$2\" \"$3\" \"${COMP_CWORD}\" \"${COMP_LINE}\" \"${COMP_WORDS[@]}\")"
        IFS=$'\n' read -r -d '' -a COMPREPLY <<< "$reply"
        local opt="${COMPREPLY[-1]}"
        unset COMPREPLY[-1]
        $opt
        return 0
    }
    complete -F _buildsetup buildsetup
    """

    print(textwrap.dedent(shellcode))


def get_mktemp():
    import subprocess
    try:
        r = subprocess.check_output(('mktemp'),stderr=subprocess.STDOUT)
        os.unlink(r.strip())
        return 'mktemp'
    except subprocess.CalledProcessError:
        pass
    r = subprocess.check_output(('mktemp','-t','buildsetup'),stderr=subprocess.STDOUT)
    os.unlink(r.strip())
    return 'mktemp -t buildsetup'


def setup_bash():
    shellcode="""\
    buildsetup(){{
        local reply="$({mktemp})"
        run-buildsetup "$reply" "$@"
        IFS=$'\n' read -r -d '' -a result < "$reply"
        rm -f "${{reply}}"
        local count=${{#result[@]}}
        if [[ count -eq 0 ]] ; then
            return
        fi
        local rerun=0
        if [[ "${{result[-1]}}" = "rerun-buildsetup" ]] ; then
            local rerun=1
            unset result[${{#result[@]}}-1]
        fi
        for cmd in "${{result[@]}}" ; do
            $cmd
        done
        if [[ $rerun -eq 0 ]] ; then
            return
        fi
        reply="$({mktemp})"
        run-buildsetup __rerun "$reply" "$@"
        IFS=$'\n' read -r -d '' -a result < "$reply"
        rm -f "${{reply}}"
        for cmd in "${{result[@]}}" ; do
            $cmd
        done
    }}
    _buildsetup(){{
        local reply="$(python -c 'from buildsetup.scripts import bash_complete; bash_complete()' \"$2\" \"$3\" \"${{COMP_CWORD}}\" \"${{COMP_LINE}}\" \"${{COMP_WORDS[@]}}\")"
        IFS=$'\n' read -r -d '' -a COMPREPLY <<< "$reply"
        local opt="${{COMPREPLY[-1]}}"
        unset COMPREPLY[${{#COMPREPLY[@]}}-1]
        $opt
        return 0
    }}
    complete -F _buildsetup buildsetup
    """
    print(textwrap.dedent(shellcode).format(mktemp=get_mktemp()))
    for arg in sys.argv:
        if arg.startswith('alias='):
            alias = arg.split('=')[1]
            print('{}() {{ buildsetup "$@" ; }} ; complete -F _buildsetup {}'.format(alias,alias))


def buildsetup():
    if len(sys.argv) < 2:
        print('buildsetup - worries about setting up your build so you don\'t have to',file=sys.stderr)
        sys.exit(0)
    elif sys.argv[1] == "__setup":
        setup_bash()
    else:
        configure()
