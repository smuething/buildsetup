import yaml, sys

from .util import AttrDict, add_yaml_representer, yamlable
from .exceptions import BuildSetupError


class PluginError(BuildSetupError):
    pass

class PluginWarning(PluginError):
    pass


@yamlable(tag=u'!buildsetup.pluginhookdispatcher',template=u'<singleton>')
class PluginHookDispatcher(object):

    __hooks = {}

    def __init__(self,cfg):
        self.__cfg = cfg

    @property
    def cfg(self):
        return self.__cfg

    @classmethod
    def register_hook(cls,hook,returns_value):
        cls.__hooks[hook] = AttrDict(name=hook,returns_value=returns_value)

    @classmethod
    def is_valid_hook(cls,hook):
        return hook in cls.__hooks.viewkeys()

    def __getattr__(self,name):
        try:
            hook = self.__hooks[name]
        except KeyError:
            raise AttributeError(name)

        if hook.returns_value:
            def invoke_hook(*args,**kwargs):
                result = {}
                for plugin in self.cfg.iterplugins():
                    result[plugin.name] = getattr(plugin,name)(*args,**kwargs)
                return result
        else:
            def invoke_hook(*args,**kwargs):
                try:
                    call = kwargs['call']
                    del kwargs['call']
                except KeyError:
                    call = lambda p,f : f(*args,**kwargs)

                try:
                    on_warning = kwargs['on_warning']
                    del kwargs['on_warning']
                except KeyError:
                    def on_warning(p,w):
                        print('==== WARNING: plugin <{}>: {}: {}'.format(p.name,w.__class__.__name__,str(w),file=sys.stderr))

                try:
                    on_error = kwargs['on_error']
                    del kwargs['on_error']
                except KeyError:
                    on_error = None

                for plugin in self.cfg.iterplugins():
                    try:
                        call(plugin,getattr(plugin,name))
                    except PluginWarning as w:
                        on_warning(plugin,w)
                    except PluginError as e:
                        if on_error:
                            if on_error(plugin,e):
                                call(plugin,getattr(plugin,name))
                        else:
                            raise

        return invoke_hook


def plugin_hook(*args,**kwargs):

    returns_value = kwargs.get('returns_value',False)

    def do_plugin_hook(f):
        PluginHookDispatcher.register_hook(f.__name__,returns_value=returns_value)
        return f

    if len(args) > 0:
        if len(args) > 1:
            raise ValueError('Parameters to @plugin_hook must be passed as named arguments')
        return do_plugin_hook(args[0])
    else:
        return do_plugin_hook


class Plugin(object):

    def __init__(self,name,cfg):
        self.__name = name
        self.__cfg = cfg
        add_yaml_representer(self.__class__,u'!buildsetup.plugin',u'<{data.name}>')

        self.cfg.register_plugin(self)

    @property
    def name(self):
        return self.__name

    @property
    def cfg(self):
        return self.__cfg

    @plugin_hook
    def create_configurator(self,configurator):
        pass

    @plugin_hook
    def finalize_configurator(self,configurator):
        pass

    @plugin_hook(returns_value=True)
    def report_configurator(self,configurator):
        return ()

    @plugin_hook
    def load_build_data(self,configurator,data):
        pass

    @plugin_hook(returns_value=True)
    def save_build_data(self,configurator):
        return None

    @plugin_hook
    def parse_config_keys(self,configurator):
        pass
