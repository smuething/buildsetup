import os, subprocess
from collections import OrderedDict
from enum import Enum

from buildsetup.util import AttrDict, memoize, EnumParser, yaml_enum
from buildsetup.plugin import Plugin, PluginError, PluginWarning
from buildsetup.exceptions import *

def load(cfg):
    return LModPlugin(cfg)

class LModError(PluginError):
    pass

class ModuleMismatchError(LModError):
    pass

class ModuleMismatchWarning(PluginWarning,LModError):
    pass

@yaml_enum
class ModuleMismatchStrategy(Enum):

    error = 1
    ignore = 2
    reload = 3


class LModPlugin(Plugin):

    def __init__(self,cfg):
        super(LModPlugin,self).__init__('lmod',cfg)
        self.processed_modules = set()

        self.cfg.register_plugin_key(
            plugin = self,
            name = 'mismatch',
            default = ModuleMismatchStrategy.error,
            optional = True,
            converter = EnumParser(ModuleMismatchStrategy)
        )



    def finalize_configurator(self,configurator):
        env = os.environ
        for name,data in self.cfg.plugins.lmod.module.viewitems():
            if data.trigger in env:
                for d in data.cmakedefines:
                    d = AttrDict(d)
                    try:
                        value = env[d.source]
                    except KeyError:
                        if d.optional:
                            continue
                        else:
                            raise LModError('Found trigger for module "{}", but required environment variable "{}" is not set'.format(name,d.source))
                    configurator._set_cmake(d.target,value)
                self.processed_modules.add(name)

    @memoize
    def loaded_modules(self):
        modules = OrderedDict(
            m.split('/') for m in
              subprocess.check_output((os.environ['SHELL'],'-c','module -q -t list'),stderr=subprocess.STDOUT).split('\n') if m
            )
        return [ AttrDict(name=n,version=v) for (n,v) in modules.viewitems() ]

    def report_configurator(self,configurator):
        return (
            'active modules: {}'.format(', '.join('{}/{}{}'.format(m.name,m.version,' (*)' if m.name in self.processed_modules else '') for m in self.loaded_modules) or '<None>'),
        )


    def save_build_data(self,configurator):
        return dict(modules=self.loaded_modules)

    def load_build_data(self,configurator,data):
        old_modules = set((m.name,m.version) for m in data.modules)
        current_modules = set((m.name,m.version) for m in self.loaded_modules)

        if old_modules != current_modules:
            strategy = self.cfg.plugins.lmod.config.mismatch.value
            if strategy == ModuleMismatchStrategy.error:
                raise ModuleMismatchError('This build was done with a different set of modules than what is loaded now!')
            elif strategy == ModuleMismatchStrategy.ignore:
                raise ModuleMismatchWarning('This build was done with a different set of modules than what is loaded now!')
            elif strategy == ModuleMismatchStrategy.reload:
                commands = []
                if len(current_modules) > 0:
                    commands.append('ml {}'.format(' '.join('-{module.name}/{module.version}'.format(module=module) for module in reversed(self.loaded_modules))))
                if len(old_modules) > 0:
                    commands.append('ml {}'.format(' '.join('{module.name}/{module.version}'.format(module=module) for module in data.modules)))
                configurator.request_rerun(
                    self,
                    reason = 'Loaded set of modules does not match set of modules used for last build',
                    commands = commands
                )

    def parse_config_keys(self,configurator):
        configurator.parse_plugin_key(self,'mismatch')
