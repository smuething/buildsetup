from __future__ import print_function

import os, sys, lya, yaml, types
from enum import Enum
from collections import Mapping
from itertools import chain

try:
    from types import StringTypes as str_types
except ImportError:
    str_types = str, bytes # py3

from .exceptions import *


class SimpleYAMLRepresenter(object):

    def __init__(self,tag,template):
        self.tag = tag
        self.template = template

    def __call__(self,dumper,data):
        return dumper.represent_scalar(self.tag,self.template.format(data=data))

def add_yaml_representer(cls,tag,template=''):
    yaml.representer.SafeRepresenter.add_representer(
        cls,SimpleYAMLRepresenter(tag,template)
    )

def yamlable(tag,template):
    def invoke(cls):
        add_yaml_representer(cls,tag,template)
        return cls
    return invoke

def yaml_enum(enum):
    return yamlable(u'!{}'.format(enum.__name__),'{data}')(enum)


class AttrDictYAMLLoader(yaml.SafeLoader):
	'Based on: https://gist.github.com/844388'

	def __init__(self, *args, **kwargs):
		super(AttrDictYAMLLoader, self).__init__(*args, **kwargs)
		self.add_constructor(u'tag:yaml.org,2002:map', type(self).construct_yaml_map)
		self.add_constructor(u'tag:yaml.org,2002:omap', type(self).construct_yaml_map)

	def construct_yaml_map(self, node):
                return AttrDict(self.construct_mapping(node))


class AttrDict(lya.AttrDict):

    def __setitem__(self, k, v):
	super(lya.AttrDict, self).__setitem__( k,
            AttrDict(v) if isinstance(v, Mapping) and not isinstance(v,AttrDict) else v )

    @classmethod
    def from_yaml(cls, path_or_file, if_exists=False):
	src_load = lambda src: cls.from_data(yaml.load(src, AttrDictYAMLLoader))
	if isinstance(path_or_file, str_types):
	    if if_exists and not os.path.exists(path_or_file):
                return cls()
	    with open(path_or_file) as src:
                return src_load(src)
	else:
            return src_load(path_or_file)

    def dump(self, stream):
        yaml.representer.SafeRepresenter.add_representer(
	    AttrDict, yaml.representer.SafeRepresenter.represent_dict )
        super(AttrDict,self).dump(stream)


def which(cmd):
    if cmd.startswith(os.pathsep):
        paths = cmd,
    else:
        paths = os.environ['PATH'].split(os.pathsep)

    for p in paths:
        f = os.path.join(p, cmd)
        if os.access(f, os.X_OK):
            return f
    raise BuildSetupError('Could not find executable "{}"'.format(cmd))


def fail(msg):
    print(msg,file=sys.stderr)
    sys.exit(1)


class memoize(object):

    def __init__(self,func):
        self.__doc__ = func.__doc__
        self.func = func

    def __get__(self,obj,cls):
        if obj is None:
            raise AttributeError(self.func.__name__)
        value = self.func(obj)
        setattr(obj,self.func.__name__,value)
        return value


@yamlable(tag=u'!buildsetup.yamlcallablewrapper',template='<{data.name}>')
class YAMLCallableWrapper(object):

    def __init__(self,callable):
        self.callable = callable
        if isinstance(callable,types.InstanceType):
            self.name = callable.__class__.__name__
        elif isinstance(callable,types.FunctionType):
            self.name = str(callable.__name__)
        elif isinstance(callable,types.MethodType):
            self.name = 'method <{}> of class <{}>'.format(callable.im_func.__name__,callable.im_class.__name__)
        else:
            self.name = callable

    def __call__(self,*args,**kwargs):
        return self.callable(*args,**kwargs)


class VariantRepoResolver(object):

    def __init__(self,cfg):
        self.cfg = cfg

    def __call__(self):
        return self.cfg.projects[self.cfg.project.value].variants


class EnumParser(object):

    def __init__(self,enum_type):
        self.enum_type = enum_type

    def __call__(self,value):
        if value in self.enum_type:
            return value
        else:
            return self.enum_type[value]

    def completions(self):
        return [ v.name for v in self.enum_type ]


class BoolParser(object):

    TRUE_VALUES  = 'true','yes','on','1'
    FALSE_VALUES = 'false','no','off','0'

    def __call__(self,value):
        if isinstance(value,bool):
            return value
        value = value.strip()
        if value.lower() in self.TRUE_VALUES:
            return True
        if value.lower() in self.FALSE_VALUES:
            return False
        raise ValueError('"{}" is not a valid bool value'.format(value))

    def completions(self):
        return chain(self.TRUE_VALUES,self.FALSE_VALUES)
