

class BuildSetupError(Exception):
    pass

class ArgumentError(BuildSetupError):
    pass

class DuplicateConfigError(BuildSetupError):
    pass

class InvalidConfigError(BuildSetupError):
    pass
