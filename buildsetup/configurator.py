from __future__ import print_function

import os, sys
from itertools import chain

from .exceptions import BuildSetupError
from .configuration import Configuration
from .util import which, AttrDict, memoize, BoolParser

class Configurator(dict):

    @classmethod
    def register_keys(cls,cfg):
        cfg.register_key(
            name = 'nested-builddir',
            default = False,
            optional = True,
            converter = BoolParser()
            )

        cfg.register_key(
            name = 'dryrun',
            default = False,
            optional = True,
            converter = BoolParser()
            )

        cfg.register_key(
            name = 'prefix',
            default = '',
            optional = True
            )

        cfg.register_key(
            name = 'export-env',
            default = False,
            optional = True,
            converter = BoolParser()
            )



    def __init__(self,cfg,cmdfile,needs_rerun):
        super(Configurator,self).__init__()
        self.cfg = cfg
        self.cmake_flags = {}
        self.need_rerun = False
        self.rerun_reasons = []
        self.shell_commands = []
        self.cmdfile = cmdfile
        self.needs_rerun = needs_rerun
        self.rerun_reasons = []
        self.rerun_commands = []

        self.register_keys(self.cfg)

        self.cfg.plugins.hooks.create_configurator(self)

    def __getattr__(self,name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def parse_keys(self):
        self.cfg.parse_key('verbose')

        self.cfg.parse_key('project')

        if not self.cfg.project.value in self.cfg.project.repo:
            raise InvalidConfigError('Project "{}" not found in project repository. Known projects: {}'.format(self.cfg.project.value,', '.join((p for p in self.cfg.project.repo.keys() if p != 'config'))))

        self.cfg.parse_key('platform')
        self.cfg.parse_key('toolchain')
        self.cfg.parse_key('build')
        self.cfg.parse_key('generator')
        self.cfg.parse_key('variant')

        self.cfg.parse_key('nested-builddir')
        self.cfg.parse_key('dryrun')
        self.cfg.parse_key('prefix')
        self.cfg.parse_key('export-env')

        self.cfg.plugins.hooks.parse_config_keys(self)

        #self.cfg.dump(sys.stdout)



    def _set_cmake(self,cmake_var,value):
        if cmake_var is None:
            return

        if not isinstance(cmake_var,(tuple,list)):
            cmake_var = cmake_var,

        for var in cmake_var:
            self.cmake_flags[var] = value

    def set_value(self,name,groups=None,key=None,default=Configuration.UNSPECIFIED,cmake_var=None,path=False):
        key = key or name.lower()
        value = self.cfg.get_entry(key,groups=groups,default=default)
        self[name] = value

        if path:
            value = which(value)
        self._set_cmake(cmake_var,value)


    def set_list(self,name,keys,subkey=None,groups=None,cmake_var=None):
        value = ' '.join(self.cfg.get_list(keys=keys,subkey=subkey,groups=groups))
        self[name] = value

        self._set_cmake(cmake_var,value)

    @memoize
    def build_dir(self):
        components = tuple(c.value for c in (self.cfg.generator,self.cfg.variant,self.cfg.toolchain) if not c.is_default)
        prefix = self.cfg.prefix.value
        if self.cfg['nested-builddir'].value:
            components = os.path.join(prefix,*components)
            dir = os.path.join(components,self.cfg.build.value)
        else:
            dir = '-'.join(chain(components,(self.cfg.build.value,)))
            if prefix.endswith('/'):
                dir = prefix + dir
            else:
                dir = '-'.join((prefix,dir)) if prefix else dir

        dir = os.path.abspath(os.path.realpath(dir))
        if os.path.exists(dir):
            if not os.path.isdir(dir):
                raise BuildSetupError('Cannot create build directory "{}", already exists and is not a directory!'.format(dir))
        else:
            if not self.cfg.dryrun.value:
                os.makedirs(dir)
        return dir

    def finalize(self):

        self.cfg.plugins.hooks.finalize_configurator(self)

        self['CMAKE_FLAGS'] = ' '.join((
            ' '.join('-D{}="{}"'.format(*v) for v in self.cmake_flags.iteritems()),
            ' '.join('-D{}="{}"'.format(*v) for l in self.cfg.get_list(keys='cmakedefines') for v in l.iteritems()),
            ' '.join(self.cfg.get_list(keys='cmakeflags'))
        ))

    def report(self,s):
        print("""\
project  : {cfg.project.value} ({cfg.project.source})
variant  : {cfg.variant.value} ({cfg.variant.source})
platform : {cfg.platform.value} ({cfg.platform.source})
generator: {cfg.generator.value} ({cfg.generator.source})
toolchain: {cfg.toolchain.value} ({cfg.toolchain.source})
build    : {cfg.build.value} ({cfg.build.source})
nested   : {cfg[nested-builddir].value} ({cfg[nested-builddir].source})
builddir : {builddir}""".format(cfg=self.cfg,builddir=self.build_dir),file=s)
        for p,r in self.cfg.plugins.hooks.report_configurator(self).viewitems():
            if len(r) > 0:
                print('------ plugin <{}> ------'.format(p),file=s)
                for line in r:
                    print(line,file=s)

        if self.cfg.verbose.value > 0:
            for k, v in sorted(self.items(),key=lambda i : i[0]):
                print('{} = {}'.format(k,v),file=s)

    def buildinfo_file(self):
        return os.path.join(self.build_dir,'buildinfo.yaml')

    def load_build_data(self):
        if os.path.isfile(self.buildinfo_file()):
            build_data = AttrDict.from_yaml(self.buildinfo_file())

            def load_plugin_data(plugin,hook):
                if plugin.name in build_data.plugin:
                    hook(self,build_data.plugin[plugin.name])

            self.cfg.plugins.hooks.load_build_data(call=load_plugin_data)

    def save_build_data(self,s):
        data = self.cfg.plugins.hooks.save_build_data(self)
        build_data = AttrDict(plugin=AttrDict())
        for p,d in data.viewitems():
            if d is not None:
                build_data.plugin[p] = d
        if self.cfg.dryrun.value:
            print('Saving of build data skipped in simulated mode',file=s)
            if (self.cfg.verbose.value > 0):
                print('Dump of build data:',file=s)
                build_data.dump(s)
        else:
            with open(self.buildinfo_file(),'w') as datafile:
                build_data.dump(datafile)


    def parse_plugin_key(self,plugin,key):
        self.cfg.parse_key('{}.{}'.format(plugin.name,key))


    def request_rerun(self,plugin,reason,commands):
        self.need_rerun = True
        self.rerun_reasons.append((plugin,reason))
        self.rerun_commands.append((plugin,commands))


    def run(self,s,args=sys.argv):
        if self.need_rerun:
            print('============= RERUN REQUIRED ==============',file=s)
            for (plugin,reason) in self.rerun_reasons:
                print('plugin <{}>: {}'.format(plugin.name,reason),file=s)
            print('------------- ACTIONS ---------------------',file=s)
            print('The following commands will be executed in your shell before re-running buildsetup:',file=s)
            with open(self.cmdfile,'w') as cmds:
                for (plugin,commands) in self.rerun_commands:
                    for command in commands:
                        print('plugin <{}>: {}'.format(plugin.name,command),file=s)
                        print(command,file=cmds)
                print('rerun-buildsetup',file=cmds)
        else:
            args = ["dune-common/bin/dunecontrol","--builddir={}".format(self.build_dir)]
            args.extend(sys.argv[1:])
            if self.cfg.dryrun.value:
                with open(self.cmdfile,'w') as cmds:
                    if self.cfg['export-env'].value:
                        for var,value in self.viewitems():
                            print('echo export {}="{}"'.format(var,value),file=cmds)
                    print('echo ' + ' '.join(args),file=cmds)
            else:
                env = dict(os.environ)
                if self.cfg['export-env'].value:
                    with open(self.cmdfile,'w') as cmds:
                        for var,value in self.viewitems():
                            print('export {}="{}"'.format(var,value),file=cmds)
                            print(' '.join(args),file=cmds)
                else:
                    env.update(self)
                    os.execve("dune-common/bin/dunecontrol",args,env)
