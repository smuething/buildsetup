from __future__ import print_function

import os, sys, lya, yaml
from collections import OrderedDict, Mapping
from pkg_resources import iter_entry_points
from itertools import chain

from .exceptions import *
from .util import AttrDict, VariantRepoResolver, YAMLCallableWrapper
from .plugin import PluginHookDispatcher

def is_subkey(v,subkey):
    return isinstance(v,Mapping) and len(v) == 1 and isinstance(iter(v.viewvalues()).next(),list)

class Configuration(AttrDict):

    UNSPECIFIED = 'UNSPECIFIED'

    BUILDSETUP_CONFIG_DIR_VAR = 'BUILDSETUP_CONFIG_DIR'

    DEFAULT_CONFIG_DIR = '.buildsetup'

    CONFIG_DIR = os.environ.get(BUILDSETUP_CONFIG_DIR_VAR,os.path.join(os.environ['HOME'],DEFAULT_CONFIG_DIR))

    def config_path(self,path,isdir=False):

        if not isinstance(path,(tuple,list)):
            path = path,

        if not path[0].startswith(os.pathsep):
            p = os.path.join(Configuration.CONFIG_DIR,*path)
        else:
            p = o.path.join(*path)

        if isdir:
            if os.path.isdir(p):
                return p
            else:
                raise ArgumentError('Could not find directory "{}"'.format(p))
        else:
            if os.path.isfile(p):
                return p
            ext_path = p + '.yaml'
            if os.path.isfile(ext_path):
                return ext_path
            else:
                raise ArgumentError('Could not find file "{}"'.format(p))

    def config_files(self,dir_path,skip):

        if not isinstance(dir_path,(tuple,list)):
            dir_path = dir_path,

        if not isinstance(skip,(tuple,list)):
            skip = skip,

        p = self.config_path(path=dir_path,isdir=True)

        if not os.path.exists(p):
            raise StopIteration

        if not os.path.isdir(p):
            raise ArgumentError('Expected directory at "{}"'.format(dir_path))

        configs = {}
        for base, dirs, files in os.walk(p):
            for f in (f for f in files if not f in skip):
                name, ext = os.path.splitext(f)
                if ext == '.yaml':
                    p = self.config_path(path=(base,f))
                    if name in configs:
                        raise DuplicateConfigError('Found duplicate config for "{}" at "{}" and "{}"'.format(name,configs[name],p))
                    configs[name] = p
                    yield name, p

    def collect_configs(self,dir_path,enforce_top_level_key=True,base_key=None,prefix=(),target=None,skip=()):

        if not isinstance(dir_path,(tuple,list)):
            dir_path = dir_path,

        if not isinstance(prefix,(tuple,list)):
            prefix = prefix,

        base_key = dir_path[-1] + 's' if base_key is None else base_key
        target = target or self

        data = AttrDict()
        for name, path in self.config_files(dir_path=dir_path,skip=skip):

            if name in skip:
                continue

            data.update_yaml(path)
            h = data
            try:
                for c in prefix:
                    h = h[c]
            except KeyError:
                data.dump(sys.stdout)
                raise InvalidConfigError('Configuration "{}" in file "{}" must have prefix "{}"'.format(name,path,'.'.join(prefix)))

            if enforce_top_level_key and not name in h:
                data.dump(sys.stdout)
                raise InvalidConfigError('Configuration "{}" in file "{}" must contain top level key of same name'.format(name,path))

        if base_key:
            target.update_dict({ base_key : data })
        else:
            target.update_dict(data)

    def collect_projects(self,dir_path):

        if not 'projects' in self:
            self['projects'] = AttrDict()

        for base, dirs, files in os.walk(self.config_path(path=dir_path,isdir=True),followlinks=True):

            if any(os.path.splitext(f)[1] == '.yaml' for f in files):
                raise InvalidConfigError('The project directory "{}" may only contain project subdirectories, but no config files)'.format(path))

            for d in dirs:
                pf = self.config_path(path=(base,d,d))
                data = AttrDict.from_yaml(pf)

                try:
                    data['project'][d]
                except KeyError:
                    raise InvalidConfigError('The project file "{}" does not contain the required key "project.{}"'.format(pf,d))

                # collect variants
                self.collect_configs(dir_path=(base,d),base_key=False,prefix=('project',d,'variant'),target=data,skip=d)

                # fix up naming
                data['project'][d]['variants'] = data['project'][d].get('variant',AttrDict())
                try:
                    del data['project'][d]['variant']
                except KeyError:
                    pass

                self.update_dict(data)

            # Only consider direct child directories of project dir
            break

        # fix up naming
        if 'project' in self:
            self.projects.update_dict(self.project)
            del self['project']


    def __init__(self):
        super(Configuration,self).__init__()

        self.configkeys = AttrDict()

        self.register_key(name='project')
        self.register_key(name='platform')
        self.register_key(name='toolchain')
        self.register_key(name='build')
        self.register_key(name='generator')
        self.register_key(name='variant',repo=VariantRepoResolver(self),lazy_repo=True,optional=True)
        self.register_key(name='verbose',default=0,optional=True,converter=int)

        # the master and per-directory configuration files are optional, so don't fail if we can't find them
        for path in ('config.yaml',),(os.environ['PWD'],'buildsetup.yaml'):
            try:
                config_file = self.config_path(path=path)
            except ArgumentError:
                pass
            else:
                self.update_yaml(config_file)

        self.load_plugins()

        self.collect_configs(dir_path='platform')
        self.collect_configs(dir_path='toolchain')
        self.collect_configs(dir_path='build')
        self.collect_configs(dir_path='generator')
        self.collect_projects(dir_path='project')

    @classmethod
    def from_data(cls, data=None):
        return AttrDict.from_data(data)

    @classmethod
    def from_yaml(cls, path_or_file, if_exists=False):
        return AttrDict.from_yaml(path_or_file,if_exists)

    def dump(self, stream):
	yaml.representer.SafeRepresenter.add_representer(
            Configuration, yaml.representer.SafeRepresenter.represent_dict )
        super(Configuration,self).dump(stream)

    def default_groups(self):
        return tuple(g for g in (self.platform,self.toolchain,self.generator,self.project,self.variant,self.build) if g.value)

    def walk_inheritance(self,key):

        try:
            e = key.repo[key.value]
        except KeyError:
            raise InvalidConfigError('Could not find configuration file for "{}={}". Valid values are: {}'.format(key.name,key.value,', '.join(key.repo.keys())))

        yield key.value, e

        while "inherit" in e:
            name = e.inherit
            e = key.repo[name]
            yield name,e

    def get_list(self,keys,subkey=None,groups=None):

        if not isinstance(keys,(tuple,list)):
            keys = keys,

        groups = groups or self.default_groups()

        dicts = []

        seen_groups = []

        for d in groups:
            hierarchy = tuple(reversed(tuple(self.walk_inheritance(d))))
            dicts.append((tuple(h[1] for h in hierarchy),seen_groups))
            seen_groups = seen_groups + [(d.name,tuple(h[0] for h in hierarchy))]

        result = []

        for group, seen in dicts:
            for d in group:
                for k in keys:
                    if "overrides" in d and k in d.overrides:
                        overrides = d.overrides[k]
                        result = [ overrides[i] if i in overrides else i for i in result ]

                    for cfg in chain(
                            (d,),
                            (d[group_name][group_value]
                             for (group_name,group_values) in seen if group_name in d
                             for group_value in group_values if group_value in d[group_name])):
                        if k in cfg:
                            if subkey is not None:
                                for i in (v for v in cfg[k] if is_subkey(v,subkey)):
                                    if subkey in i:
                                        result.extend(i[subkey])
                            else:
                                result.extend(v for v in cfg[k] if not is_subkey(v,subkey))

        return [i for i in result if i is not None]

    def get_entry(self,key,groups=None,default=UNSPECIFIED):

        groups = groups or self.default_groups()

        groups = groups or self.default_groups()

        dicts = []

        seen_groups = []

        for d in groups:
            hierarchy = tuple(reversed(tuple(self.walk_inheritance(d))))
            dicts.append((tuple(h[1] for h in hierarchy),seen_groups))
            seen_groups = seen_groups + [(d.name,tuple(h[0] for h in hierarchy))]

        for group, seen in reversed(dicts):
            for d in reversed(group):
                for cfg in chain(
                        (d,),
                        (d[group_name][group_value]
                         for (group_name,group_values) in seen if group_name in d
                         for group_value in group_values if group_value in d[group_name])):
                    if key in cfg:
                        return cfg[key]

        if default is Configuration.UNSPECIFIED:
            raise KeyError('Key "{}" not found in configuration!'.format(key))
        else:
            return default


    def register_key(self,name,optional=False,default=None,repo=None,target=None,converter=lambda v : v.lower(),
                     completions=None,lazy_repo=False,shortname=None):

        if completions is None:
            try:
                completions = YAMLCallableWrapper(converter.completions)
            except AttributeError:
                completions = YAMLCallableWrapper(lambda : ())

        if repo is None:
            self[name + 's'] = repo = AttrDict()

        self.configkeys[name] = AttrDict(
            name=name,
            optional=optional,
            default=default,
            repo=YAMLCallableWrapper(repo) if lazy_repo else repo,
            target=target,
            converter=YAMLCallableWrapper(converter),
            completions=completions,
            lazy_repo=lazy_repo,
            shortname=shortname or name
            )
        self.configkeys[name].target = target

    def register_plugin_key(self,plugin,name,optional=False,default=None,repo=None,target=None,converter=lambda v : v.lower(),
                            completions=None,lazy_repo=False):
        self.register_key(
            name = '{}.{}'.format(plugin.name,name),
            optional = optional,
            default = default,
            repo = repo,
            target = self.plugins[plugin.name].config,
            converter = converter,
            completions = completions,
            lazy_repo = lazy_repo,
            shortname=name
        )

    def parse_key(self,name,env=os.environ,argv=sys.argv):

        options = self.configkeys[name]
        value = options.default
        is_default = True
        set_in_config = False
        source = 'default'
        repo = None

        try:
            value = self.config[name]
            set_in_config = True
            source = 'global configuration'
        except (AttributeError, KeyError):
            pass

        try:
            value = self.directory.config[name]
            set_in_config = True
            source = 'per-directory configuration'
        except (AttributeError, KeyError):
            pass

        try:
            value = env['BUILDSETUP_' + name.upper().replace('-','_').replace('.','_')]
            is_default = False
            source = 'environment'
        except KeyError:
            pass

        newargv = []
        pattern = '{}='.format(name)
        for arg in argv:
            if arg.startswith(pattern):
                value = arg[len(pattern):]
                is_default = False
                source = 'command line'
            else:
                newargv.append(arg)

        argv[:] = newargv

        # check to see whether there is a repo
        try:
            repo = options.repo or self[name + 's']
        except KeyError:
            pass

        if options.lazy_repo:
            repo = repo()

        value = options.converter(value) if value is not None else value

        if repo:
            try:
                if repo[value].abstract:
                    raise InvalidConfigError('Chosen value "{}" for "{}" is abstract'.format(value,name))
            except KeyError:
                pass

        target = options.target
        if target is None:
            target = self

        target[options.shortname] = AttrDict(value=value,is_default=is_default,name=options.shortname,repo=repo,source=source)

        if is_default and not set_in_config and not options.optional:
            raise ArgumentError('You must specify the parameter "{}"'.format(name))

        return is_default


    def load_plugins(self,env=os.environ,argv=sys.argv):

        name = 'plugins'
        plugin_list = []

        try:
            plugin_list.extend(self.config[name])
        except (AttributeError, KeyError):
            pass

        try:
            plugin_list.extend(self.directory.config[name])
        except (AttributeError, KeyError):
            pass

        try:
            env_list = env['BUILDSETUP_PLUGINS']
            plugin_list.extend(env_list.split(','))
        except KeyError:
            pass

        newargv = []
        pattern = '{}='.format(name)
        for arg in argv:
            if arg.startswith(pattern):
                argv_list = arg[len(pattern):]
                plugin_list.extend(argv_list.split(','))
            else:
                newargv.append(arg)

        argv[:] = newargv

        # remove duplicates while preserving load order
        plugin_list = list(OrderedDict.fromkeys(plugin_list))

        self['plugins'] = AttrDict(hooks=PluginHookDispatcher(self))

        # load plugins
        for plugin in plugin_list:
            entry_point = iter_entry_points('buildsetup.plugins',plugin).next()
            loader = entry_point.load()
            loader(self)


    def register_plugin(self,plugin):
        self.plugins[plugin.name] = AttrDict(plugin=plugin,config=AttrDict())
        self.collect_configs(dir_path=('plugins',plugin.name),enforce_top_level_key=False,base_key=False,target=self.plugins[plugin.name])


    def iterplugins(self):
        for p in self.plugins.viewvalues():
            if isinstance(p,PluginHookDispatcher):
                continue
            yield p.plugin
